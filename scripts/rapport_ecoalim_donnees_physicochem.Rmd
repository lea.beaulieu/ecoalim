---
title: "Rapport pour les données physico-chimiques"
author : Léa Beaulieu
date: "`r format(Sys.time(), '%d, %B, %Y')`"
output: 
  html_document: 
    code_folding: hide
    css: bioconductor.css
    highlight: pygments
    number_sections: yes
    toc: yes
    toc_depth: 6
    toc_float:
      collapsed: yes
      scroll_smooth: no
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
	eval = TRUE,
	echo = FALSE,
	fig.align = "center",
	fig.height = 5,
	fig.width = 7,
	message = FALSE,
	warning = FALSE
)
```

```{r loading_package, include=FALSE}
library(BiocManager)
library(tidyverse)
library(FactoMineR)
library(factoextra)
library(here)
```

```{r setting_path, include=FALSE, results='hide'}
here::i_am("scripts/rapport_ecoalim_donnees_physicochem.Rmd")
data <- here("data")

```

```{r data_loading}
table_all <- read.csv("C:/Users/lbeaulieu/Documents/ecoalim/data/donnees_physicochem.csv", sep = ';', header = T)
rownames(table_all) <- table_all$Id
table_all <- table_all[-13,]

convert_num <- function(x) as.numeric(as.character(x))
i <- c(8:62)

table_all[, i] <- apply(table_all[,i], 2, 
                    function(x) as.numeric(as.character(x)))
```

# Pca all conditions

```{r pca_all}
acp <- prcomp(table_all[, -c(1:7)], scale. = T, center = T)
my_df <- cbind(table_all[,1:7], acp$x)
percent_Var <- (acp$sdev^2) / sum(acp$sdev^2)

fviz_pca_var(acp, axes = c(1, 2), label = "var", select.var = list(contrib = 20), col.var =  "contrib", repel = T, title = "Correlation circle for all conditions") +
  scale_color_gradient2(low="white", mid="blue",
      high="red", space = "Lab")

ggplot(data = my_df, aes(x = PC1, y = PC2, label = Id, color = repetition, shape = Lot)) +
  geom_point(size = 2.5) +
  xlab(paste0("PC1: ", round(percent_Var[1] * 100), "% variance")) +
  ylab(paste0("PC2: ", round(percent_Var[2] * 100), "% variance")) +
  labs(title = "PCA with all conditions and labels") +
  theme(plot.title = element_text(size = 14, face = "bold"),
        axis.title.x = element_text(size = 13, face = "bold"),
        axis.title.y = element_text(size = 13, face = "bold")) + 
  geom_text() +
  scale_color_brewer(name = "Parents", palette = "Set2")+
  scale_shape_manual(values = c(0, 16))

```

# PCA per batch

```{r pca_batch}
for(i in unique(table_all$Lot)){
  tablei <- table_all[table_all$Lot == i,]
  acp <- PCA(tablei[,-c(1:7)], 
           scale.unit = T, 
           ncp = 5, 
           graph = FALSE)
  my_df <- cbind(tablei[,1:7], acp$ind$coord)
  eigenvalues <- acp$eig

  c <- fviz_pca_var(acp, axes = c(1, 2), label = "var", select.var = list(contrib = 20), col.var =  "contrib", repel = T, title = paste0("Correlation circle for experiment ", i)) +
    scale_color_gradient2(low="white", mid="blue",
    high="red", space = "Lab")
  print(c)

  p <- ggplot(data = my_df, aes(x = Dim.1, y = Dim.2, label = Id, color = repetition)) +
  geom_point(size = 2.5) +
  labs(title = paste0("PCA for batch ", i, " with all physico-chemical components")) +
  theme(plot.title = element_text(size = 14, face = "bold"),
       axis.title.x = element_text(size = 13, face = "bold"),
       axis.title.y = element_text(size = 13, face = "bold")) + 
  geom_text() +
  xlab(paste0("PC1: ", 
              round(eigenvalues[1,2]), 
              "% variance")) +
  ylab(paste0("PC2: ", 
              round(eigenvalues[2,2]), 
              "% variance")) +
  scale_color_brewer(name = "Parents", palette = "Set2")
  print(p)
}
```

# PCA per groups 

## PCA feces

```{r per_feces}
feces <- select(table_all, contains("feces"))
feces <- feces[,-10]
acp_f <- prcomp(feces, scale. = T, center = T)
my_df_f <- cbind(table_all[,1:7], acp_f$x)
percent_Var_f <- (acp_f$sdev^2) / sum(acp_f$sdev^2)


fviz_pca_var(acp_f, axes = c(1, 2), label = "var", select.var = list(contrib = 20), col.var =  "contrib", repel = T, title = "Correlation circle for feces") +
  scale_color_gradient2(low="white", mid="blue",
      high="red", space = "Lab")

ggplot(data = my_df_f, aes(x = PC1, y = PC2, label = Id, color = Lot)) +
  geom_point(size = 2.5) +
  xlab(paste0("PC1: ", round(percent_Var_f[1] * 100), "% variance")) +
  ylab(paste0("PC2: ", round(percent_Var_f[2] * 100), "% variance")) +
  labs(title = "PCA for feces") +
  theme(plot.title = element_text(size = 14, face = "bold"),
        axis.title.x = element_text(size = 13, face = "bold"),
        axis.title.y = element_text(size = 13, face = "bold")) + 
  geom_text() +
  scale_color_brewer(name = "Lot", palette = "Set2")

```

## PCA urine

```{r per_urines}
urines <- select(table_all, contains("urine"))
urines <- urines[,-3]
acp_u <- prcomp(urines, scale. = T, center = T)
my_df_u <- cbind(table_all[,1:7], acp_u$x)
percent_Var_u <- (acp_u$sdev^2) / sum(acp_u$sdev^2)


fviz_pca_var(acp_u, axes = c(1, 2), label = "var", select.var = list(contrib = 20), col.var =  "contrib", repel = T, title = "Correlation circle for urines") +
  scale_color_gradient2(low="white", mid="blue",
      high="red", space = "Lab")

ggplot(data = my_df_u, aes(x = PC1, y = PC2, label = Id, color = Lot)) +
  geom_point(size = 2.5) +
  xlab(paste0("PC1: ", round(percent_Var_u[1] * 100), "% variance")) +
  ylab(paste0("PC2: ", round(percent_Var_u[2] * 100), "% variance")) +
  labs(title = "PCA for urines") +
  theme(plot.title = element_text(size = 14, face = "bold"),
        axis.title.x = element_text(size = 13, face = "bold"),
        axis.title.y = element_text(size = 13, face = "bold")) + 
  geom_text() +
  scale_color_brewer(name = "Lot", palette = "Set2")

```

## PCA alim

```{r per_alim}
alim <- select(table_all, contains("alim"))
acp_a <- prcomp(alim, scale. = T, center = T)
my_df_a <- cbind(table_all[,1:7], acp_a$x)
percent_Var_a <- (acp_a$sdev^2) / sum(acp_a$sdev^2)


fviz_pca_var(acp_a, axes = c(1, 2), label = "var", select.var = list(contrib = 20), col.var =  "contrib", repel = T, title = "Correlation circle for ingested food") +
  scale_color_gradient2(low="white", mid="blue",
      high="red", space = "Lab")

ggplot(data = my_df_a, aes(x = PC1, y = PC2, label = Id, color = Lot)) +
  geom_point(size = 2.5) +
  xlab(paste0("PC1: ", round(percent_Var_a[1] * 100), "% variance")) +
  ylab(paste0("PC2: ", round(percent_Var_a[2] * 100), "% variance")) +
  labs(title = "PCA for ingested food") +
  theme(plot.title = element_text(size = 14, face = "bold"),
        axis.title.x = element_text(size = 13, face = "bold"),
        axis.title.y = element_text(size = 13, face = "bold")) + 
  geom_text() +
  scale_color_brewer(name = "Lot", palette = "Set2")

```

## PCA gaz

```{r per_gaz}
gaz <- table_all[,c(32:34, 47:50)]
acp_g <- prcomp(gaz, scale. = T, center = T)
my_df_g <- cbind(table_all[,1:7], acp_g$x)
percent_Var_g <- (acp_g$sdev^2) / sum(acp_g$sdev^2)


fviz_pca_var(acp_g, axes = c(1, 2), label = "var", select.var = list(contrib = 20), col.var =  "contrib", repel = T, title = "Correlation circle for gaz") +
  scale_color_gradient2(low="white", mid="blue",
      high="red", space = "Lab")

ggplot(data = my_df_g, aes(x = PC1, y = PC2, label = Id, color =  Lot)) +
  geom_point(size = 2.5) +
  xlab(paste0("PC1: ", round(percent_Var_g[1] * 100), "% variance")) +
  ylab(paste0("PC2: ", round(percent_Var_g[2] * 100), "% variance")) +
  labs(title = "PCA for gaz") +
  theme(plot.title = element_text(size = 14, face = "bold"),
        axis.title.x = element_text(size = 13, face = "bold"),
        axis.title.y = element_text(size = 13, face = "bold")) + 
  geom_text() +
  scale_color_brewer(name = "Lot", palette = "Set2")

```

## PCA slurry

```{r per_slurry}
lisier <- table_all[,c(51:62)]
acp_l <- prcomp(lisier, scale. = T, center = T)
my_df_l <- cbind(table_all[,1:7], acp_l$x)
percent_Var_l <- (acp_l$sdev^2) / sum(acp_l$sdev^2)


fviz_pca_var(acp_l, axes = c(1, 2), label = "var", select.var = list(contrib = 20), col.var =  "contrib", repel = T, title = "Correlation circle for slurry") +
  scale_color_gradient2(low="white", mid="blue",
      high="red", space = "Lab")

ggplot(data = my_df_l, aes(x = PC1, y = PC2, label = Id, color =  Lot)) +
  geom_point(size = 2.5) +
  xlab(paste0("PC1: ", round(percent_Var_l[1] * 100), "% variance")) +
  ylab(paste0("PC2: ", round(percent_Var_l[2] * 100), "% variance")) +
  labs(title = "PCA for slurry") +
  theme(plot.title = element_text(size = 14, face = "bold"),
        axis.title.x = element_text(size = 13, face = "bold"),
        axis.title.y = element_text(size = 13, face = "bold")) + 
  geom_text() +
  scale_color_brewer(name = "Lot", palette = "Set2")

```